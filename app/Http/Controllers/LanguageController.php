<?php

namespace App\Http\Controllers;
use App\Http\Resources\LanguageResource;
use App\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index()
    {
        $lang = Language::all();
        return LanguageResource::collection($lang);
    }
}
