<?php

use Illuminate\Database\Seeder;
use App\Language;


class LangaugeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create( [
            'language_name' => 'English',
            'language_code' => 'en',
        ] );

        Language::create( [
            'language_name' => 'Spanish',
            'language_code' => 'es',
        ] );

        Language::create( [
            'language_name' => 'Chinese',
            'language_code' => 'zh',
        ] );

        Language::create( [
            'language_name' => 'German',
            'language_code' => 'de',
        ] ); 

        Language::create( [
            'language_name' => 'French',
            'language_code' => 'fr',
        ] );        
    }
}
